package com.example.jsj.devolva_me;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import telas.menu.Main2Activity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botao = (Button) findViewById(R.id.buttonLogar);

        assert botao != null;

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //pegando dados da view
                EditText login = (EditText) findViewById(R.id.editTextLogin);
                EditText senha = (EditText) findViewById(R.id.editTextSenha);

                //transformando em string
                String loginString = login.getText().toString();
                String senhaString = senha.getText().toString();

                if (loginString.equals("jadis") && senhaString.equals("2710")) {
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Login errado", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
