package telas.menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.jsj.devolva_me.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 29/05/2016.
 */
public class ListaPessoas extends AppCompatActivity {

    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> pessoasList;
    private static String url_consultas_pessoas = "http://webfate.esy.es/android/get_all_pessoas.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PESSOAS = "pessoas";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "nome";
    private static final String TAG_TELEFONE = "telefone";
    JSONArray empresaJson = null;
    ListView lv;
    ListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_pessoas);
        pessoasList = new ArrayList<>();
        new LoadSemuaBukuTamu().execute();
        lv = (ListView) findViewById(R.id.listView);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pid = ((TextView) view.findViewById(R.id.single_post_tv_id)).getText()
                        .toString();

                Intent in = new Intent(getApplicationContext(),
                        EditarPessoa.class);
                in.putExtra(TAG_PID, pid);
                startActivity(in);
                finish();
            }
        });
    }

    class LoadSemuaBukuTamu extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = ListaPessoas.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();

            JSONObject json = jParser.makeHttpRequest(url_consultas_pessoas, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_PESSOAS);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);

                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);
                        String telefone = c.getString(TAG_TELEFONE);

                        HashMap<String, String> map = new HashMap<>();

                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_TELEFONE, telefone);


                        pessoasList.add(map);
                    }
                } else {
                    Intent i = new Intent(getApplicationContext(),
                            CadastrarPessoa.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * json atualizado para ArrayList
                     * */
                     adapter = new SimpleAdapter(
                            ListaPessoas.this, pessoasList,
                            R.layout.single_post, new String[] { TAG_PID,
                            TAG_NAME, TAG_TELEFONE},
                            new int[] { R.id.single_post_tv_id,
                                    R.id.single_post_tv_nome,
                                    R.id.single_post_tv_telefone });

                    lv.setAdapter(adapter);

                    EditText filter =
                            (EditText) findViewById(R.id.edtTextPessoa);

                    assert filter != null;
                    filter.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            ((SimpleAdapter) ListaPessoas.this.adapter).getFilter().filter(s);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                }
            });


        }

    }

}
