package telas.menu;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.jsj.devolva_me.R;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.JSONParser;

public class CadastrarObjeto extends AppCompatActivity {

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    ArrayList<String> pessoasList;

    public CadastrarObjeto(){
        pessoasList = new ArrayList<>();
    }

    static final int DATE_DIALOG_ID = 0;
    EditText inputName;
    ImageButton inputData;
    EditText label;

    String nome;
    String categoria;
    String data = "";
    String fk_pessoa;

    private static final String TAG_SUCCESS = "success";

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastrar_objeto);

        label = (EditText) findViewById(R.id.dataLabel);
        label.setEnabled(false);

        Intent intent = getIntent();
        if(intent.getExtras().getStringArrayList("itens") == null){
            Intent i = new Intent(getApplicationContext(), CadastrarPessoa.class);
            startActivity(i);
        } else {
            pessoasList = intent.getExtras().getStringArrayList("itens");
        }

        inputName = (EditText) findViewById(R.id.editTexNomeobjeto);
        inputData = (ImageButton) findViewById(R.id.editTextData);
        ArrayAdapter<String> aCategorias;
        ArrayAdapter<String> aPessoas;
        Spinner spinner;
        Spinner spinner2;

        aPessoas = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, pessoasList);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        assert spinner2 != null;
        spinner2.setAdapter(aPessoas);
        spinner2.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent2, View view, int position, long id) {
                        fk_pessoa = parent2.getItemAtPosition(position).toString();
                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        Toast.makeText(getApplicationContext(), "Selecione alguma pessoa", Toast.LENGTH_LONG).show();
                    }
                }
        );

        String[] categorias = {"Livro", "CD/DVD", "Eletronicos", "Eletrodomesticos", "Vestuario"};
        aCategorias = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categorias);
        spinner = (Spinner) findViewById(R.id.spinner);
        assert spinner != null;
        spinner.setAdapter(aCategorias);
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
                        categoria = parent.getItemAtPosition(posicao).toString();
                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        Toast.makeText(getApplicationContext(), "Selecione alguma categoria", Toast.LENGTH_LONG).show();
                    }
                }
        );


        inputData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        inputData.requestFocus();
        ImageButton salvarObjeto = (ImageButton) findViewById(R.id.dsadsa);

        assert salvarObjeto != null;
        salvarObjeto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nome = inputName.getText().toString();

                if(nome.length() > 0 && nome != null){
                    if(categoria != null){
                        if(fk_pessoa != null){
                            if(data.length() == 10 || data == null){
                                new CadastrarNovoObjeto().execute();
                            } else {
                                Toast.makeText(getApplicationContext(), "Erro: campo data insuficiente", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Erro: campo pessoa insuficiente", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Erro: campo categoria insuficiente", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Erro: campo nome insuficiente", Toast.LENGTH_LONG).show();
                }
            }

        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    //metodos aqui
    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calendario = Calendar.getInstance();

        int ano = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, ano, mes,
                        dia);
        }
        return null;
    }


    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String d = String.valueOf(dayOfMonth);
            String m =  String.valueOf(monthOfYear+1);
            String a =  String.valueOf(year);

            String dia = null;
            String mes = null;

            if (d.length() < 2) {
                dia = "0"+d;
            } else {
                dia = d;
            }

            if(m.length() < 2) {
                mes = "0"+m;
            } else {
                mes = m;
            }


            label.setText(dia + "/" + mes + "/" + a);
            data = dia + "/" + mes + "/" + a;
        }
    };

    class CadastrarNovoObjeto extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CadastrarObjeto.this);
            pDialog.setMessage("Cadastrando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("nome", nome));
            params.add(new BasicNameValuePair("categoria", categoria));
            params.add(new BasicNameValuePair("data", data));
            params.add(new BasicNameValuePair("fkpessoa", fk_pessoa));

            String url_salvar_objeto = "http://webfate.esy.es/android/create_objetos.php";
            JSONObject json = jsonParser.
                    makeHttpRequest(url_salvar_objeto,
                            "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    finish();
                    Intent i = new Intent(getApplicationContext(), ListarObjeto.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "Erro ao cadastrar objeto, verifique sua conexao", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }

}
