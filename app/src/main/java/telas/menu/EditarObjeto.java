package telas.menu;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.jsj.devolva_me.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.JSONParser;

public class EditarObjeto extends AppCompatActivity {

    EditText txtNomeObjeto;
    EditText txtCategoriaObjeto;
    ImageButton btnDataObjeto;
    EditText txtFkpessoaObjeto;
    ImageButton btnAlterar;
    ImageButton btnDeletar;
    ImageButton btnMenu;
    String id;
    EditText label;
    private AlertDialog alerta;

    private ProgressDialog pDialog;
    static final int DATE_DIALOG_ID = 0;

    JSONParser jsonParser = new JSONParser();

    private static final String url_objeto_id = "http://webfate.esy.es/android/get_objetos_details.php";
    private static final String url_pessoas_alterar = "http://webfate.esy.es/android/update_objetos.php";
    private static final String url_objeto_deletar = "http://webfate.esy.es/android/delete_objetos.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_OBJETOS = "objetos";
    private static final String TAG_PID = "id";
    private static final String TAG_NOME = "nome";
    private static final String TAG_CATEGORIA = "categoria";
    private static final String TAG_DATA = "data";
    private static final String TAG_FKPESSOA = "fkpessoa";
    private static final String TAG_DEVOLVIDO = "devolvido";

    static String name;
    static String categoria;
    static String data;
    static String fkpessoa;
    static Integer devolvido;
    private CheckBox checkBox;

    public void voltarParaListaObjeto(){
        Intent intent = new Intent(getApplicationContext(), ListarObjeto.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_objeto);

        label = (EditText) findViewById(R.id.labelEditData);
        label.setEnabled(false);


        btnMenu = (ImageButton) findViewById(R.id.buttonMenu);
        btnAlterar = (ImageButton) findViewById(R.id.buttonEditarObjeto);
        btnDeletar = (ImageButton) findViewById(R.id.buttonRemoverObjeto);
        checkBox = (CheckBox) findViewById(R.id.checkBoxDevolvido);
        txtNomeObjeto = (EditText) findViewById(R.id.editTextNomeObjetoEditar);
        txtCategoriaObjeto = (EditText) findViewById(R.id.editTextCategoriaObjetoEditar);
        txtFkpessoaObjeto = (EditText) findViewById(R.id.editTextPessoaObjetoEditar);
        btnDataObjeto = (ImageButton) findViewById(R.id.editTextDataObjetoEditar);
        txtNomeObjeto.requestFocus();

        txtCategoriaObjeto.setEnabled(false);
        txtFkpessoaObjeto.setEnabled(false);

        Intent i = getIntent();
        id = i.getStringExtra(TAG_PID);

        new GetObjeto().execute();

        btnDataObjeto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(i);
            }
        });



        btnAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = txtNomeObjeto.getText().toString();

                if(checkBox.isChecked()){
                    Log.i("Devolvido", "1");
                    devolvido = 1;
                } else {
                    Log.i("Devolvido", "0");
                    devolvido = 0;
                }
                if(name.length() > 2 && !name.equals("")){
                    if(data.length() > 2 && !data.equals("")){
                        new AlteraObjeto().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Campo DATA insuficiente", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Campo NOME insuficiente", Toast.LENGTH_LONG).show();

                }
            }
        });

        btnDeletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditarObjeto.this);
                builder.setTitle("Confirma��o");
                builder.setMessage("Excluir este arquivo?");
                builder.setPositiveButton("Positivo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        new ExcluirObjeto().execute();
                    }
                });
                builder.setNegativeButton("Negativo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
                alerta = builder.create();
                alerta.show();
            }
        });
    }

    //metodos aqui
    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calendario = Calendar.getInstance();

        int ano = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, ano, mes,
                        dia);
        }
        return null;
    }


    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String d = String.valueOf(dayOfMonth);
            String m =  String.valueOf(monthOfYear+1);
            String a =  String.valueOf(year);

            String dia = null;
            String mes = null;

            if (d.length() < 2) {
                dia = "0"+d;
            } else {
                dia = d;
            }

            if(m.length() < 2) {
                mes = "0"+m;
            } else {
                mes = m;
            }


            label.setText(dia + "/" + mes + "/" + a);
            data = dia + "/" + mes + "/" + a;
        }
    };

    class GetObjeto extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarObjeto.this);
            pDialog.setMessage("Carregando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {
                        List<NameValuePair> params = new ArrayList<>();
                        params.add(new BasicNameValuePair("id", id));
                        params.add(new BasicNameValuePair("nome", name));
                        params.add(new BasicNameValuePair("categoria", categoria));
                        params.add(new BasicNameValuePair("data", data));
                        params.add(new BasicNameValuePair("fkpessoa", fkpessoa));

                        StrictMode.ThreadPolicy policy =
                                new StrictMode.ThreadPolicy.Builder().
                                        permitAll().build();

                        StrictMode.setThreadPolicy(policy);

                        JSONObject json = jsonParser.makeHttpRequest(
                                url_objeto_id, "GET", params);

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {
                            JSONArray productObj = json
                                    .getJSONArray(TAG_OBJETOS);
                            JSONObject pendaftaran = productObj
                                    .getJSONObject(0);

                            txtNomeObjeto.setText(pendaftaran.getString(TAG_NOME));
                            txtCategoriaObjeto.setText(pendaftaran.getString(TAG_CATEGORIA));
                            txtFkpessoaObjeto.setText(pendaftaran.getString(TAG_FKPESSOA));
                            label.setText(pendaftaran.getString(TAG_DATA));
                            data = (TAG_DATA);

                        } else {
                            Toast.makeText(getApplicationContext(), "N�o foi poss�vel concluir a a��o.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class AlteraObjeto extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarObjeto.this);
            pDialog.setMessage("Carregando dados ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair(TAG_PID, id));
            params.add(new BasicNameValuePair(TAG_NOME, name));
            params.add(new BasicNameValuePair(TAG_DATA, data));
            params.add(new BasicNameValuePair(TAG_DEVOLVIDO, String.valueOf(devolvido)));

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            JSONObject json = jsonParser.makeHttpRequest(
                    url_pessoas_alterar, "POST", params);
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();
                    voltarParaListaObjeto();
                } else {
                    Toast.makeText(getApplicationContext(), "N�o foi poss�vel concluir a a��o.", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class ExcluirObjeto extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarObjeto.this);
            pDialog.setMessage("Excluindo...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            int success;
            try {
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("id", id));

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                JSONObject json = jsonParser.makeHttpRequest(
                        url_objeto_deletar, "POST", params);
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();
                    voltarParaListaObjeto();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }
}
