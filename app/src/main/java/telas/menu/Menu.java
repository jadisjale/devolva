package telas.menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.jsj.devolva_me.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 28/05/2016.
 */
public class Menu extends AppCompatActivity {

    JSONArray empresaJson = null;
    JSONParser jsonParser = new JSONParser();
    private static String url_consultas_pessoas = "http://webfate.esy.es/android/get_all_pessoas.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PESSOAS = "pessoas";
    private static final String TAG_NAME = "nome";
    private static final String TAG_PESSOAS_BANCO = "pessoas";
    List<String> pessoasList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            Toast.makeText(getApplicationContext(), "Lista de objetos vazia", Toast.LENGTH_LONG).show();
        }
        Button cadastrarPessoa = (Button) findViewById(R.id.btCadastrarPessoa);

        Button buttonDevolvido = (Button) findViewById(R.id.buttonDevolvido);

        Button pesquisarObjeto = (Button) findViewById(R.id.buttonPesquisarObjeto);

        assert buttonDevolvido != null;
        buttonDevolvido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListaObjetoDevolvido.class);
                startActivity(intent);
            }
        });

        assert pesquisarObjeto != null;
        pesquisarObjeto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListarObjeto.class);
                startActivity(intent);
            }
        });

        assert cadastrarPessoa != null;
        cadastrarPessoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CadastrarPessoa.class);
                startActivity(intent);
            }
        });

        Button cadastrarObjeto = (Button) findViewById(R.id.btCadastrarObjeto);

        assert cadastrarObjeto != null;
        cadastrarObjeto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoadSemuaBukuTamu().execute();
            }
        });

        Button pesquisarPessoa = (Button) findViewById(R.id.buttonPesquisarPessoa);

        assert pesquisarPessoa != null;
        pesquisarPessoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, ListaPessoas.class);
                startActivity(intent);
            }
        });
    }

    class LoadSemuaBukuTamu extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = Menu.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista de pessoas...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();

            JSONObject json = jsonParser.makeHttpRequest(url_consultas_pessoas, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    pessoasList = new ArrayList<>();
                    empresaJson = json.getJSONArray(TAG_PESSOAS);
                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);
                        String name = c.getString(TAG_NAME);
                        pessoasList.add(name);
                    }
                } else {
                    pessoasList = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            Intent intent;
            if(pessoasList == null){
                intent = new Intent(Menu.this, CadastrarPessoa.class);
                startActivity(intent);
            } else {
                intent = new Intent(Menu.this, CadastrarObjeto.class);
                intent.putStringArrayListExtra("itens", new ArrayList<>(pessoasList));
                startActivity(intent);
            }
        }
    }
}
