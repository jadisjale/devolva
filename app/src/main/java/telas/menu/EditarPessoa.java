package telas.menu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.jsj.devolva_me.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import json.JSONParser;

/**
 * Created by jsj on 29/05/2016.
 */
public class EditarPessoa extends AppCompatActivity {

    EditText txtNomePessoa;
    EditText txtTelefonePessoa;
    ImageButton btnAlterar;
    ImageButton btnMenu;
    ImageButton btnDeletar;
    String id;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_pessoa_id = "http://webfate.esy.es/android/get_pessoas_details.php";

    private static final String url_pessoas_alterar = "http://webfate.esy.es/android/update_pessoas.php";

    private static final String url_pessoa_deletar = "http://webfate.esy.es/android/delete_pessoas.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PESSOAS = "pessoas";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "nome";
    private static final String TAG_TELEFONE = "telefone";

    private AlertDialog alerta;

    static String name;
    static String telefone;

    public void listar(){
        Intent i = new Intent(getApplicationContext(), ListaPessoas.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_pessoa);

        btnAlterar = (ImageButton) findViewById(R.id.buttonEditarPessoa);
        btnDeletar = (ImageButton) findViewById(R.id.buttonRemoverPessoa);
        btnMenu = (ImageButton) findViewById(R.id.imageButtonMenu);

        txtNomePessoa = (EditText) findViewById(R.id.editTextNomePessoaEditar);
        txtTelefonePessoa = (EditText) findViewById(R.id.editTextTelefoneEditar);
        txtNomePessoa.requestFocus();

        MaskEditTextChangedListener maskTEL = new MaskEditTextChangedListener("(##)#####-####", txtTelefonePessoa);
        txtTelefonePessoa.addTextChangedListener(maskTEL);

        Intent i = getIntent();
        id = i.getStringExtra(TAG_PID);

        new GetPessoa().execute();

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(i);
            }
        });

        btnAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = txtNomePessoa.getText().toString();
                telefone = txtTelefonePessoa.getText().toString();
                if(name.length() > 2 && !name.equals("")){
                    if(telefone.length() > 2 && !telefone.equals("")){
                        new AlteraPessoa().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Campo TELEFONE insuficiente", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Campo NOME insuficiente", Toast.LENGTH_LONG).show();

                }
            }
        });

        btnDeletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditarPessoa.this);
                builder.setTitle("Confirmação");
                builder.setMessage("Excluir este arquivo?");
                builder.setPositiveButton("Positivo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        new ExcluirPessoa().execute();
                    }
                });
                builder.setNegativeButton("Negativo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
                alerta = builder.create();
                alerta.show();
            }
        });
    }

    class GetPessoa extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarPessoa.this);
            pDialog.setMessage("Carregando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("id", id));
                        params.add(new BasicNameValuePair("nome", name));
                        params.add(new BasicNameValuePair("telefone", telefone));

                        StrictMode.ThreadPolicy policy =
                                new StrictMode.ThreadPolicy.Builder().
                                        permitAll().build();

                        StrictMode.setThreadPolicy(policy);

                        JSONObject json = jsonParser.makeHttpRequest(
                                url_pessoa_id, "GET", params);

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {
                            JSONArray productObj = json
                                    .getJSONArray(TAG_PESSOAS);
                            JSONObject pendaftaran = productObj
                                    .getJSONObject(0);

                            txtNomePessoa.setText(pendaftaran.getString(TAG_NAME));
                            txtTelefonePessoa.setText(pendaftaran.getString(TAG_TELEFONE));

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class AlteraPessoa extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarPessoa.this);
            pDialog.setMessage("Carregando dados ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair(TAG_PID, id));
            params.add(new BasicNameValuePair(TAG_NAME, name));
            params.add(new BasicNameValuePair(TAG_TELEFONE, telefone));

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            JSONObject json = jsonParser.makeHttpRequest(
                    url_pessoas_alterar, "POST", params);
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();
                    listar();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class ExcluirPessoa extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditarPessoa.this);
            pDialog.setMessage("Excluindo...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            int success;
            try {
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("id", id));

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                JSONObject json = jsonParser.makeHttpRequest(
                        url_pessoa_deletar, "POST", params);
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();
                    listar();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }
}
