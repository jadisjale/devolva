package telas.menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.jsj.devolva_me.MainActivity;
import com.example.jsj.devolva_me.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import json.JSONParser;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    JSONArray empresaJson = null;
    JSONParser jsonParser = new JSONParser();
    private static String url_consultas_pessoas = "http://webfate.esy.es/android/get_all_pessoas.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PESSOAS = "pessoas";
    private static final String TAG_NAME = "nome";
    private static final String TAG_PESSOAS_BANCO = "pessoas";
    List<String> pessoasList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            Toast.makeText(getApplicationContext(), "Lista de objetos vazia", Toast.LENGTH_LONG).show();
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), CadastrarPessoa.class);
                startActivity(i);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent intent;

        if (id == R.id.cadastrarPessoa) {
            intent = new Intent(getApplicationContext(), CadastrarPessoa.class);
            startActivity(intent);
        } else if (id == R.id.cadastrarObjeto) {
           new LoadSemuaBukuTamu().execute();
        } else if (id == R.id.pesquisarPessoa) {
            intent = new Intent(getApplicationContext(), ListaPessoas.class);
            startActivity(intent);
        } else if (id == R.id.pesquisarObjeto) {
            intent = new Intent(getApplicationContext(), ListarObjeto.class);
            startActivity(intent);
        } else if (id == R.id.pesquisarObjetoDevolvido) {
            intent = new Intent(getApplicationContext(), ListaObjetoDevolvido.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class LoadSemuaBukuTamu extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = Main2Activity.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista de pessoas...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();

            JSONObject json = jsonParser.makeHttpRequest(url_consultas_pessoas, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    pessoasList = new ArrayList<>();
                    empresaJson = json.getJSONArray(TAG_PESSOAS);
                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);
                        String name = c.getString(TAG_NAME);
                        pessoasList.add(name);
                    }
                } else {
                    pessoasList = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            Intent intent;
            if(pessoasList == null){
                intent = new Intent(Main2Activity.this, CadastrarPessoa.class);
                startActivity(intent);
            } else {
                intent = new Intent(Main2Activity.this, CadastrarObjeto.class);
                intent.putStringArrayListExtra("itens", new ArrayList<>(pessoasList));
                startActivity(intent);
            }
        }
    }
}
