package telas.menu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.jsj.devolva_me.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.jansenfelipe.androidmask.MaskEditTextChangedListener;
import json.JSONParser;

public class CadastrarPessoa extends AppCompatActivity {

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();
    EditText inputName;
    EditText inputTelefone;
    String nome;
    String telefone;

    private static String url_salvar_pessoa = "http://webfate.esy.es/android/create_pessoas.php";


    private static final String TAG_SUCCESS = "success";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastrar_pessoa);

        inputName = (EditText) findViewById(R.id.editTextNomePessoa);
        inputTelefone = (EditText) findViewById(R.id.editTextTelefone);
        inputName.requestFocus();
        MaskEditTextChangedListener maskTEL = new MaskEditTextChangedListener("(##)#####-####", inputTelefone);
        inputTelefone.addTextChangedListener(maskTEL);

        ImageButton salvarPessoa = (ImageButton) findViewById(R.id.btCadastrarPessoa);

        assert salvarPessoa != null;
        salvarPessoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nome = inputName.getText().toString();
                telefone = inputTelefone.getText().toString();
                if(nome.length() > 2 && !nome.equals("")){
                    if(telefone.length() > 2 && !telefone.equals("")){
                        new CadastrarNewPessoa().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), "Campo TELEFONE insuficiente", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Campo NOME insuficiente", Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    class CadastrarNewPessoa extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CadastrarPessoa.this);
            pDialog.setMessage("Cadastrando dados...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("nome", nome));
            params.add(new BasicNameValuePair("telefone", telefone));

            JSONObject json = jsonParser.
                    makeHttpRequest(url_salvar_pessoa,
                            "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    finish();
                    Intent i = new Intent(getApplicationContext(), ListaPessoas.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "Erro ao cadastrar empresa, verifique sua conexao", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }
}
