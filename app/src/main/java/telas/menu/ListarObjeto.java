package telas.menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.jsj.devolva_me.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import json.JSONParser;

/**
 * Created by jsj on 30/05/2016.
 */
public class ListarObjeto extends AppCompatActivity {

    ListAdapter adapter;
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> objetosList;
    private static String url_consultas_objetos = "http://webfate.esy.es/android/get_all_objetos.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_OBJETOS = "objetos";
    private static final String TAG_PID = "id";
    private static final String TAG_NAME = "nome";
    private static final String TAG_CATEGORIA = "categoria";
    private static final String TAG_FKPESSOA = "fkpessoa";
    JSONArray empresaJson = null;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_objetos);

        objetosList = new ArrayList<>();
        new LoadSemuaBukuTamu().execute();
        lv = (ListView) findViewById(R.id.listView2);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pid = ((TextView) view.findViewById(R.id.single_post_tv_id_objeto)).getText()
                        .toString();

                Intent in = new Intent(getApplicationContext(),
                        EditarObjeto.class);
                in.putExtra(TAG_PID, pid);
                startActivity(in);
                finish();
            }
        });

    }

    class LoadSemuaBukuTamu extends AsyncTask<String, String, String> {

        private ProgressDialog pDialog;
        private Context context = ListarObjeto.this;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Carregando lista...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * obter todas as empresas a partir da url get em background
         * */
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<>();

            JSONObject json = jParser.makeHttpRequest(url_consultas_objetos, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    empresaJson = json.getJSONArray(TAG_OBJETOS);

                    for (int i = 0; i < empresaJson.length(); i++) {
                        JSONObject c = empresaJson.getJSONObject(i);

                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);
                        String categoria = c.getString(TAG_CATEGORIA);
                        String fkpessoa = c.getString(TAG_FKPESSOA);

                        HashMap<String, String> map = new HashMap<>();

                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_CATEGORIA, categoria);
                        map.put(TAG_FKPESSOA, fkpessoa);

                        objetosList.add(map);
                    }
                } else {
                    Intent intent = new Intent(getApplicationContext(),
                            Main2Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("txt", true);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            if (this.pDialog.isShowing()) {
                this.pDialog.dismiss();
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * json atualizado para ArrayList
                     * */
                     adapter = new SimpleAdapter(
                            ListarObjeto.this, objetosList,
                            R.layout.single_post_objeto, new String[] { TAG_PID,
                            TAG_NAME, TAG_CATEGORIA, TAG_FKPESSOA},
                            new int[] { R.id.single_post_tv_id_objeto,
                                    R.id.single_post_tv_nome_objeto,
                                    R.id.single_post_tv_categoria_objeto,
                                    R.id.single_post_tv_pessoa_objeto});
                    lv.setAdapter(adapter);

                    EditText filter =
                            (EditText) findViewById(R.id.edtText);

                    assert filter != null;

                    filter.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            ((SimpleAdapter)ListarObjeto.this.adapter).getFilter().filter(s);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                }
            });


        }

    }

}
